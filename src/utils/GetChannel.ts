import axios from 'axios';

const getChannel = async () => {
  try {
    let val = await axios.get(`http://localhost:5000/getChannel`)
    console.log(val)
    return val.data
  } catch(err) {
    throw new Error(err)
  }
}

export { getChannel }